<?php
/**
 * @file
 * sitra.features.inc
 */

/**
 * Implements hook_node_info().
 */
function sitra_node_info() {
  $items = array(
    'sitra_event' => array(
      'name' => t('Evenement Sitra'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Nom de l\'événement'),
      'help' => '',
    ),
  );
  return $items;
}
