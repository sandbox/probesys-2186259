Sitra
=============

# Informations

Ce module est redige en français car il s'agit d'une application qui utilise la base de donnees Sitra propre a la Region Rhone Alpes.

Ce module permet de recuperer les donnees Sitra au format JSON en utilisant l'export fourni par Sitra.

Le principe du module est le suivant:

- Configurer la gestion des exports Sitra (activer, format JSON, url de notification, Frequence,emails de notification ). Dans le module l'url de notification est la suivante: `www.monsite.com/sitra/export`
- Sitra envoie des donnees POST a l'url de notification
- Le module recupere les informations concernant l'export et les insere dans une table nommee sitra notamment l'url de recuperation des donnees
- Le module telecharge l'archive, la decompresse dans le dossier `sites/default/files/export`, le dossier nomme de la date de l'export et le numero de lexport (ex:20130625-0426)
- Une fois les donnees decompressees, le programme va parcourir toute l'arborescence du dossier et inserer/modifier/supprimer les donnees dans Drupal

# Sitra

## API

[Documentation API Sitra 2](http://www.sitra-rhonealpes.com/wiki/index.php/API_Sitra_2)
[Manuel Utilisateurs](http://www.sitra-rhonealpes.com/wiki/index.php/Manuel_utilisateurs)

## Informations supplementaires

La plateforme Sitra est une base d'informations en ligne. Elle permet de creer et de diffuser de l'information touristique. Elle est co-alimentee par l'ensemble des acteurs du tourisme et de loisirs de 9 departements de l'est de la france. Libres, fiables et actualisees, les donnees a disposition sont des contenus editoriaux et multimedia a forte valeur ajoutee. Elles permettent a tous de valoriser l'offre des destinations dans des projets de toutes natures.

[Sitra-Tourisme.com](http://www.sitra-tourisme.com/)

# Installation

1. Installer le module

Celui-ci va creer un type de contenu `evenement_sitra` ainsi que des vues grâce au module features

2. Creer une table `sitra` qui enregistre les evenements d'export

```SQL
CREATE TABLE IF NOT EXISTS `sitra` (
  `export_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Identifiant de l’export',
  `siteWebId` varchar(255) DEFAULT '' COMMENT 'Identifiant du site internet',
  `statut` varchar(255) DEFAULT '' COMMENT 'Etat de l’export',
  `ponctuel` varchar(255) DEFAULT '' COMMENT 'Indique si l’export est ponctuel (true) ou periodique (false).',
  `reinitialisation` varchar(255) DEFAULT '' COMMENT 'Indique si l’export est une reinitialisation (true) ou differentiel (false).',
  `urlRecuperation` varchar(255) DEFAULT '' COMMENT 'L’URL de recuperation du fichier d’export.',
  `urlConfirmation` varchar(255) DEFAULT '' COMMENT 'L’URL de confirmation.',
  PRIMARY KEY (`export_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Informations concernant les exports' AUTO_INCREMENT=68 ;
```

3. Changer le path de recuperation des donnees sitra dans la fonction `_check_data`

4. Modifier le path pour enregistrer les logs en cas de probleme de recuperation d'image dans la fonction `sitra_decode_json`


# Contributing

This is a [Sandbox Project](https://drupal.org/node/1011196).

For more informations on how to contribute to drupal, [read this](https://drupal.org/contribute/development)

# License

See `Licence.txt`

# Author

Si vous avez des questions, n'hesitez pas a me contacter via mail [Yann Picot](mailto:yann.picot@probesys.com)

[ProbeSYS](http://probesys.com)

